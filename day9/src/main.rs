use std::fs;
use std::convert::TryFrom;
use std::convert::TryInto;

fn main() -> Result<(), std::io::Error> {
    println!("Part One");
    part1()?;
    println!("Part Two");
    part2()
}

fn part1() -> Result<(), std::io::Error> {
    let file_contents = std::env::args().nth(1)
        .and_then(|filename| fs::read_to_string(filename).ok())
        .expect("Need to be passed valid filename as first argument");
    let program = file_contents.trim().split(',')
        .map(|num| num.parse::<i64>())
        .collect::<Result<Vec<_>, _>>()
        .expect("Couldn't parse string as int");

    let mut computer = Computer::new(&program);
    let res = computer.run_program_to_halt(&[1]);
    println!("Output of computer: {:?}", res);

    Ok(())
}

fn part2() -> Result<(), std::io::Error> {
    let file_contents = std::env::args().nth(1)
        .and_then(|filename| fs::read_to_string(filename).ok())
        .expect("Need to be passed valid filename as first argument");
    let program = file_contents.trim().split(',')
        .map(|num| num.parse::<i64>())
        .collect::<Result<Vec<_>, _>>()
        .expect("Couldn't parse string as int");

    let mut computer = Computer::new(&program);
    let res = computer.run_program_to_halt(&[2]);
    println!("Output of computer: {:?}", res);

    Ok(())
}

fn convert_index(index: i64) -> usize {
    usize::try_from(index).expect("Couldn't convert index from i64 to usize")
}

struct Computer {
    program: Vec<i64>,
    instruction_pointer: usize,
    relative_base: i64,
    halted: bool
}

impl Computer {
    fn new(program: &[i64]) -> Computer {
        let mut program_vec = Vec::new();
        for p in program {
            program_vec.push(*p);
        }
        Computer {
            program: program_vec,
            instruction_pointer: 0,
            relative_base: 0,
            halted: false
        }
    }

    fn get_from_program(&mut self, opcode: i64, arg_num: usize) -> i64 {
        let mode = (opcode % 10_i64.pow(2_u32 + (arg_num as u32))) / 10_i64.pow(1_u32 + (arg_num as u32));
        let val = self.program[self.instruction_pointer + arg_num];
        match mode {
            0 => {
                let converted_val = convert_index(val);
                self.program.get(converted_val)
                    .map(|&i| i)
                    .or_else(|| {
                        self.program.resize(converted_val + 1, 0);
                        Some(0)
                    }).unwrap()
            }
            1 => val,
            2 => {
                let converted_val = convert_index(val + self.relative_base);
                self.program.get(converted_val)
                    .map(|&i| i)
                    .or_else(|| {
                        self.program.resize(converted_val + 1, 0);
                        Some(0)
                    }).unwrap()
            }
            _ => panic!("Unrecognised mode in opcode {} for arg_num {}", opcode, arg_num)
        }
    }

    fn write_to_program(&mut self, index: i64, opcode: i64, arg_num: usize, val: i64) {
        let mode = (opcode % 10_i64.pow(2_u32 + (arg_num as u32))) / 10_i64.pow(1_u32 + (arg_num as u32));
        match mode {
            0 => {
                if index >= (self.program.len() as i64) {
                    self.program.resize((index as usize) + 1, 0);
                }
                self.program[convert_index(index)] = val;
            }
            1 => panic!("Program incorrectly asks to write to an address with immediate mode"),
            2 => {
                let index = index + self.relative_base;
                if index >= (self.program.len() as i64) {
                    self.program.resize((index as usize) + 1, 0);
                }
                self.program[convert_index(index)] = val;
            }
            _ => panic!("Unrecognised mode in opcode {} for arg_num {}", opcode, arg_num)
        }
    }

    fn run_program_to_halt(&mut self, inputs: &[i64]) -> Vec<i64> {
        let mut outputs = Vec::new();
        let mut inputs_to_pass = inputs;
        loop {
            let res = self.run_program_to_output(inputs_to_pass);
            match res {
                None => return outputs,
                Some((output, remaining_inputs)) => {
                    inputs_to_pass = remaining_inputs;
                    outputs.push(output);
                }
            }
        }
    }

    fn run_program_to_output<'a>(&mut self, inputs: &'a[i64]) -> Option<(i64, &'a[i64])> {
        if self.halted {return Option::None}
        let mut input_pointer = 0;

        loop {
            let opcode = self.program[self.instruction_pointer];
            match opcode % 100 {
                1 => {
                    let b_val = self.get_from_program(opcode, 1);
                    let c_val = self.get_from_program(opcode, 2);
                    let a = self.program[self.instruction_pointer + 3];
                    self.write_to_program(a, opcode, 3, b_val + c_val);
                    self.instruction_pointer += 4;
                }
                2 => {
                    let b_val = self.get_from_program(opcode, 1);
                    let c_val = self.get_from_program(opcode, 2);
                    let a = self.program[self.instruction_pointer + 3];
                    self.write_to_program(a, opcode, 3, b_val * c_val);
                    self.instruction_pointer += 4;
                }
                3 => {
                    let b = self.program[self.instruction_pointer + 1];
                    self.write_to_program(b, opcode, 1, *inputs.get(input_pointer).expect("Program tried to get input that wasn't provided"));
                    input_pointer += 1;
                    self.instruction_pointer += 2;
                }
                4 => {
                    let b_val = self.get_from_program(opcode, 1);
                    self.instruction_pointer += 2;
                    return Option::Some((b_val, &inputs[input_pointer..]));
                }
                5 => {
                    let b_val = self.get_from_program(opcode, 1);
                    let c_val = self.get_from_program(opcode, 2);
                    if b_val != 0 {
                        self.instruction_pointer = convert_index(c_val);
                    } else {
                        self.instruction_pointer += 3;
                    }
                }
                6 => {
                    let b_val = self.get_from_program(opcode, 1);
                    let c_val = self.get_from_program(opcode, 2);
                    if b_val == 0 {
                        self.instruction_pointer = convert_index(c_val);
                    } else {
                        self.instruction_pointer += 3;
                    }
                }
                7 => {
                    let a = self.program[self.instruction_pointer + 3];
                    let b_val = self.get_from_program(opcode, 1);
                    let c_val = self.get_from_program(opcode, 2);
                    self.write_to_program(a, opcode, 3, if b_val < c_val {1} else {0});
                    self.instruction_pointer += 4;

                }
                8 => {
                    let a = self.program[self.instruction_pointer + 3];
                    let b_val = self.get_from_program(opcode, 1);
                    let c_val = self.get_from_program(opcode, 2);
                    self.write_to_program(a, opcode, 3, if b_val == c_val {1} else {0});
                    self.instruction_pointer += 4;

                }
                9 => {
                    self.relative_base = (self.relative_base + self.get_from_program(opcode, 1)).try_into().unwrap();
                    self.instruction_pointer += 2;
                }
                99 => {
                    self.halted = true;
                    return Option::None;
                }
                _ => panic!("Unexpected opcode")
            }
        }
    }
}
