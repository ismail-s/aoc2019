use std::fs;
use std::vec::Vec;
use std::collections::HashSet;

fn main() -> Result<(), std::io::Error> {
    println!("Part One");
    part1()?;
    println!("Part Two");
    part2()
}

fn part1() -> Result<(), std::io::Error> {
    let lines = get_file_lines();
    let first_line = lines.get(0).unwrap().split(',');
    let second_line = lines.get(1).unwrap().split(',');

    let vec = expand_instructions_to_coords(first_line);
    let vec1 = expand_instructions_to_coords(second_line);

    let mut set = HashSet::with_capacity(vec.len());
    for v in vec {
        set.insert(v);
    }

    let mut set1 = HashSet::with_capacity(vec1.len());
    for v in vec1 {
        set1.insert(v);
    }

    let distance_to_closest_intersection = set.intersection(&set1)
        .map(|(x, y)| x.abs() + y.abs())
        .min();
    println!("Distance to closest intersection: {:?}", distance_to_closest_intersection);

    Ok(())
}

fn part2() -> Result<(), std::io::Error> {
    let lines = get_file_lines();

    let first_line = lines.get(0).unwrap().split(',');
    let second_line = lines.get(1).unwrap().split(',');

    let vec = expand_instructions_to_coords(first_line);
    let vec1 = expand_instructions_to_coords(second_line);

    let mut set = HashSet::with_capacity(vec.len());
    for v in &vec {
        set.insert(v);
    }

    let mut set1 = HashSet::with_capacity(vec1.len());
    for v in &vec1 {
        set1.insert(v);
    }

    let intersections = set.intersection(&set1);

    let fewest_combined_steps = intersections.map(|&intersection| {
        // Look for the intersection in each vec and note the position of it
        let index = &vec.iter().position(|&x| x == *intersection).unwrap() + 1;
        let index1 = &vec1.iter().position(|&x| x == *intersection).unwrap() + 1;
        // Add up the positions
        let combined_steps = index + index1;
        println!("For intersection {:?}, combined steps are {}", intersection, combined_steps);
        combined_steps
    }).min();
    println!("Fewest combined steps: {:?}", fewest_combined_steps);

    Ok(())
}

fn get_file_lines() -> Vec<String> {
    std::env::args().nth(1)
        .and_then(|filename| fs::read_to_string(filename).ok())
        .expect("Need to be passed valid filename as first argument")
        .lines()
        .map(String::from)
        .collect()
}

fn expand_instructions_to_coords<'a>(instructions: impl std::iter::Iterator<Item=&'a str>) -> Vec<(i32, i32)> {
    let mut vec = Vec::new();
    let mut current_loc: (i32, i32) = (0, 0);

    for instruction in instructions {
        let instr = instruction.chars().nth(0).unwrap();
        let distance: i32 = instruction[1..].parse().unwrap();
        match instr {
            'U' => {
                for e in 0..distance {
                    vec.push((current_loc.0, current_loc.1 + e + 1));
                }
                current_loc.1 += distance;
            }
            'D' => {
                for e in 0..distance {
                    vec.push((current_loc.0, current_loc.1 - e - 1));
                }
                current_loc.1 -= distance;
            }
            'L' => {
                for e in 0..distance {
                    vec.push((current_loc.0 - e - 1, current_loc.1));
                }
                current_loc.0 -= distance;
            }
            'R' => {
                for e in 0..distance {
                    vec.push((current_loc.0 + e + 1, current_loc.1));
                }
                current_loc.0 += distance;
            }
            _ => panic!("Unexpected instruction")
        }
    }
    vec
}
