use std::fs;
use multimap::MultiMap;

fn main() -> Result<(), std::io::Error> {
    println!("Part One");
    part1()?;
    println!("Part Two");
    part2()
}

fn part1() -> Result<(), std::io::Error> {
    let file_contents = std::env::args().nth(1)
        .and_then(|filename| fs::read_to_string(filename).ok())
        .expect("Need to be passed valid filename as first argument");
    let program = file_contents.lines()
        .map(|string| (&string[..3], &string[4..]))
        .collect::<Vec<_>>();

    let mut graph = MultiMap::new();
    for (orbited, orbiting) in program {
        graph.insert(orbited, orbiting);
    }

    let (total_nodes, total_orbits) = count_all_orbits_in_graph(&graph, "COM");
    println!("Total nodes: {}", total_nodes);
    println!("Total orbits: {}", total_orbits);

    Ok(())
}

fn part2() -> Result<(), std::io::Error> {
    let file_contents = std::env::args().nth(1)
        .and_then(|filename| fs::read_to_string(filename).ok())
        .expect("Need to be passed valid filename as first argument");
    let program = file_contents.lines()
        .map(|string| (&string[..3], &string[4..]))
        .collect::<Vec<_>>();

    let mut graph = MultiMap::new();
    for (orbited, orbiting) in program {
        graph.insert(orbited, orbiting);
    }

    let chain_to_you = get_chain_to(&graph, "YOU");
    let chain_to_santa = get_chain_to(&graph, "SAN");

    for (i, planet) in chain_to_you.iter().enumerate() {
        if chain_to_santa.contains(planet) {
            let min_chain_to_you = chain_to_you.get(..i).unwrap();
            let min_chain_to_santa = chain_to_santa.get(..=chain_to_santa.iter().position(|x| x == planet).unwrap()).unwrap();
            println!("Minimum no. of orbital transfers required: {}", min_chain_to_you.len() + min_chain_to_santa.len() - 1);
            break;
        }
    }

    Ok(())
}

fn count_all_orbits_in_graph(graph: &MultiMap<&str, &str>, planet_name: &str) -> (i32, i32) {
    let moons_opt = graph.get_vec(planet_name);
    match moons_opt {
        None => (1, 0),
        Some(moons) => {
            let (direct, curr_total) = moons.iter().map(|moon| count_all_orbits_in_graph(&graph, moon))
                .fold((0, 0), |(direct, curr_total), (direct1, curr_total1)| (direct + direct1, curr_total + curr_total1));
            (direct + 1, curr_total + direct)
        }
    }
}

fn get_chain_to<'a>(graph: &MultiMap<&'a str, &'a str>, planet_name: &str) -> Vec<&'a str> {
    for (orbited, orbiting) in graph.iter_all() {
        if orbiting.contains(&planet_name) {
            let mut chain = get_chain_to(&graph, &orbited);
            chain.insert(0, orbited);
            return chain;
        }
    }
    Vec::new()
}
