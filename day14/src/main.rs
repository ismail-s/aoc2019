use std::fs;
use std::collections::HashMap;

fn main() -> Result<(), std::io::Error> {
    println!("Part 1");
    part1()?;
    println!("Part 2");
    part2()
}

fn part1() -> Result<(), std::io::Error> {
    let result = solve_equations_for_fuel(1)?;
    println!("Final answer is {} ORE is required.", result);
    Ok(())
}

fn part2() -> Result<(), std::io::Error> {
    // I manually tried changing this number to get the
    // maximum ORE amount that was just less than (or equal to) 1 trillion.
    let answer = 1120408;
    let result = solve_equations_for_fuel(answer)?;
    assert!(result <= 1_000_000_000_000);
    let too_large_result = solve_equations_for_fuel(answer + 1)?;
    assert!(too_large_result > 1_000_000_000_000);
    println!("Final answer is {} FUEL can be produced.", answer);
    Ok(())
}

fn solve_equations_for_fuel(fuel_amount: usize) -> Result<usize, std::io::Error> {
    let file_contents = std::env::args().nth(1)
        .and_then(|filename| fs::read_to_string(filename).ok())
        .expect("Need to be passed valid filename as first argument");
    let num_of_lines = file_contents.lines().count();
    let map = file_contents.trim().lines().map(|line| {
        let i = line.find(" => ").expect("Parse error");
        let first = &line[..i];
        let second = &line[i + " => ".len()..];
        let j = second.find(' ').expect("Parse error");
        let key = String::from(&second[j + 1..]);
        let val1 = &second[..j].parse::<usize>().expect("Parse error");
        let val2 = first.split(", ").map(|reagent| {
            let k = reagent.find(' ').expect("Parse error");
            let amount = &reagent[..k].parse::<usize>().expect("Parse error");
            let name = &reagent[k + 1..];
            (String::from(name), *amount)
        }).collect();
        (key, (*val1, val2))
    }).collect::<HashMap<String, (usize, Vec<(String, usize)>)>>();
    assert_eq!(num_of_lines, map.len());

    let mut remainders: HashMap<String, usize> = HashMap::new();
    let mut results = HashMap::new();
    let mut products_to_decompose = HashMap::new();
    products_to_decompose.insert(String::from("FUEL"), fuel_amount);
    while products_to_decompose.len() != 0 {
        let mut new_products_to_decompose = HashMap::new();
        for (product, &amount_of_product) in &products_to_decompose {
            let (eqn_factor, reagents) = match map.get(product) {
                None => {
                    *results.entry(product.clone()).or_insert(0) += amount_of_product;
                    continue
                }
                Some(res) => res
            };
            let amount_of_product = match remainders.get(product).cloned() {
                None => amount_of_product,
                Some(remainder) => {
                    if remainder == amount_of_product {
                        remainders.remove(product);
                        continue
                    } else if remainder > amount_of_product {
                        remainders.insert(product.to_string(), remainder - amount_of_product);
                        continue
                    } else {
                        amount_of_product - remainders.remove(product).unwrap()
                    }
                }
            };
            let to_mult_reagents_by = (amount_of_product + (eqn_factor - 1)) / eqn_factor;
            let excess_product = amount_of_product % eqn_factor;
            if excess_product > 0 {
                remainders.insert(product.to_string(), eqn_factor - excess_product);
            }
            reagents.iter().map(|(reagent_name, reagent_factor)|
                                (reagent_name.clone(), reagent_factor * to_mult_reagents_by))
                .for_each(|(reagent_name, amount)| *new_products_to_decompose.entry(reagent_name).or_insert(0) += amount);
        }
        products_to_decompose = new_products_to_decompose
    }

    assert_eq!(results.len(), 1);
    assert_eq!(results.contains_key("ORE"), true);
    Ok(results.get("ORE").unwrap().clone())
}
