use std::fs;
use std::convert::TryFrom;
use std::convert::TryInto;
use std::collections::HashSet;

fn main() -> Result<(), std::io::Error> {
    println!("Part One");
    part1()?;
    println!("Part Two");
    part2()
}

fn part1() -> Result<(), std::io::Error> {
    let file_contents = std::env::args().nth(1)
        .and_then(|filename| fs::read_to_string(filename).ok())
        .expect("Need to be passed valid filename as first argument");
    let program = file_contents.trim().split(',')
        .map(|num| num.parse::<i64>())
        .collect::<Result<Vec<_>, _>>()
        .expect("Couldn't parse string as int");

    let mut grid = vec![vec![0; 1000]; 1000];
    let mut current_loc = (500, 500);
    let mut direction = Direction::UP;
    let mut computer = Computer::new(&program);
    let mut painted_squares = HashSet::new();

    loop {
        let colour = match computer.run_program_to_output(&[grid[current_loc.1][current_loc.0]]) {
            None => {
                println!("Number of squares painted at least once: {}", painted_squares.len());
                return Ok(());
            }
            Some((colour, _)) => colour
        };
        let turn = computer.run_program_to_output(&[]).unwrap().0;
        // Paint squares
        grid[current_loc.1][current_loc.0] = colour;
        // Add to set of painted squares
        painted_squares.insert(current_loc);
        // Turn and move forward
        direction = do_turn(&direction, turn);
        current_loc = move_forward(current_loc, &direction);
    }
}

fn part2() -> Result<(), std::io::Error> {
    let file_contents = std::env::args().nth(1)
        .and_then(|filename| fs::read_to_string(filename).ok())
        .expect("Need to be passed valid filename as first argument");
    let program = file_contents.trim().split(',')
        .map(|num| num.parse::<i64>())
        .collect::<Result<Vec<_>, _>>()
        .expect("Couldn't parse string as int");

    let mut grid = vec![vec![0; 200]; 200];
    let mut current_loc = (100, 100);
    // Paint starting panel white
    grid[current_loc.1][current_loc.0] = 1;
    let mut direction = Direction::UP;
    let mut computer = Computer::new(&program);
    let mut painted_squares = HashSet::new();

    loop {
        let colour = match computer.run_program_to_output(&[grid[current_loc.1][current_loc.0]]) {
            None => {
                print_grid(&grid);
                return Ok(());
            }
            Some((colour, _)) => colour
        };
        let turn = computer.run_program_to_output(&[]).unwrap().0;
        // Paint squares
        grid[current_loc.1][current_loc.0] = colour;
        // Add to set of painted squares
        painted_squares.insert(current_loc);
        // Turn and move forward
        direction = do_turn(&direction, turn);
        current_loc = move_forward(current_loc, &direction);
    }
}

fn print_grid(grid: &Vec<Vec<i64>>) {
    for row in grid {
        println!("{}", row.iter().map(|&i| if i == 0 {' '} else {'█'}).collect::<String>());
    }
}

enum Direction {
    UP, DOWN, LEFT, RIGHT
}

fn do_turn(direction: &Direction, turn: i64) -> Direction {
    let turning_func = |left, right| {
        if turn == 0 {
            left
        } else if turn == 1 {
            right
        } else {
            panic!("Invalid turn instruction {}", turn)
        }
    };
    match &direction {
        Direction::UP => turning_func(Direction::LEFT, Direction::RIGHT),
        Direction::DOWN => turning_func(Direction::RIGHT, Direction::LEFT),
        Direction::LEFT => turning_func(Direction::DOWN, Direction::UP),
        Direction::RIGHT => turning_func(Direction::UP, Direction::DOWN)
    }
}

fn move_forward(current_loc: (usize, usize), direction: &Direction) -> (usize, usize) {
    let amount_to_move = match direction {
        Direction::UP => (0, -1),
        Direction::DOWN => (0, 1),
        Direction::LEFT => (-1, 0),
        Direction::RIGHT => (1, 0)
    };
    (((current_loc.0 as isize) + amount_to_move.0) as usize,
     ((current_loc.1 as isize) + amount_to_move.1) as usize)
}

fn convert_index(index: i64) -> usize {
    usize::try_from(index).expect("Couldn't convert index from i64 to usize")
}

struct Computer {
    program: Vec<i64>,
    instruction_pointer: usize,
    relative_base: i64,
    halted: bool
}

impl Computer {
    fn new(program: &[i64]) -> Computer {
        let mut program_vec = Vec::new();
        for p in program {
            program_vec.push(*p);
        }
        Computer {
            program: program_vec,
            instruction_pointer: 0,
            relative_base: 0,
            halted: false
        }
    }

    fn get_from_program(&mut self, opcode: i64, arg_num: usize) -> i64 {
        let mode = (opcode % 10_i64.pow(2_u32 + (arg_num as u32))) / 10_i64.pow(1_u32 + (arg_num as u32));
        let val = self.program[self.instruction_pointer + arg_num];
        match mode {
            0 => {
                let converted_val = convert_index(val);
                self.program.get(converted_val)
                    .map(|&i| i)
                    .or_else(|| {
                        self.program.resize(converted_val + 1, 0);
                        Some(0)
                    }).unwrap()
            }
            1 => val,
            2 => {
                let converted_val = convert_index(val + self.relative_base);
                self.program.get(converted_val)
                    .copied()
                    .or_else(|| {
                        self.program.resize(converted_val + 1, 0);
                        Some(0)
                    }).unwrap()
            }
            _ => panic!("Unrecognised mode in opcode {} for arg_num {}", opcode, arg_num)
        }
    }

    fn write_to_program(&mut self, index: i64, opcode: i64, arg_num: usize, val: i64) {
        let mode = (opcode % 10_i64.pow(2_u32 + (arg_num as u32))) / 10_i64.pow(1_u32 + (arg_num as u32));
        match mode {
            0 => {
                if index >= (self.program.len() as i64) {
                    self.program.resize((index as usize) + 1, 0);
                }
                self.program[convert_index(index)] = val;
            }
            1 => panic!("Program incorrectly asks to write to an address with immediate mode"),
            2 => {
                let index = index + self.relative_base;
                if index >= (self.program.len() as i64) {
                    self.program.resize((index as usize) + 1, 0);
                }
                self.program[convert_index(index)] = val;
            }
            _ => panic!("Unrecognised mode in opcode {} for arg_num {}", opcode, arg_num)
        }
    }

    fn run_program_to_output<'a>(&mut self, inputs: &'a[i64]) -> Option<(i64, &'a[i64])> {
        if self.halted {return Option::None}
        let mut input_pointer = 0;

        loop {
            let opcode = self.program[self.instruction_pointer];
            match opcode % 100 {
                1 => {
                    let b_val = self.get_from_program(opcode, 1);
                    let c_val = self.get_from_program(opcode, 2);
                    let a = self.program[self.instruction_pointer + 3];
                    self.write_to_program(a, opcode, 3, b_val + c_val);
                    self.instruction_pointer += 4;
                }
                2 => {
                    let b_val = self.get_from_program(opcode, 1);
                    let c_val = self.get_from_program(opcode, 2);
                    let a = self.program[self.instruction_pointer + 3];
                    self.write_to_program(a, opcode, 3, b_val * c_val);
                    self.instruction_pointer += 4;
                }
                3 => {
                    let b = self.program[self.instruction_pointer + 1];
                    self.write_to_program(b, opcode, 1, *inputs.get(input_pointer).expect("Program tried to get input that wasn't provided"));
                    input_pointer += 1;
                    self.instruction_pointer += 2;
                }
                4 => {
                    let b_val = self.get_from_program(opcode, 1);
                    self.instruction_pointer += 2;
                    return Option::Some((b_val, &inputs[input_pointer..]));
                }
                5 => {
                    let b_val = self.get_from_program(opcode, 1);
                    let c_val = self.get_from_program(opcode, 2);
                    if b_val != 0 {
                        self.instruction_pointer = convert_index(c_val);
                    } else {
                        self.instruction_pointer += 3;
                    }
                }
                6 => {
                    let b_val = self.get_from_program(opcode, 1);
                    let c_val = self.get_from_program(opcode, 2);
                    if b_val == 0 {
                        self.instruction_pointer = convert_index(c_val);
                    } else {
                        self.instruction_pointer += 3;
                    }
                }
                7 => {
                    let a = self.program[self.instruction_pointer + 3];
                    let b_val = self.get_from_program(opcode, 1);
                    let c_val = self.get_from_program(opcode, 2);
                    self.write_to_program(a, opcode, 3, if b_val < c_val {1} else {0});
                    self.instruction_pointer += 4;

                }
                8 => {
                    let a = self.program[self.instruction_pointer + 3];
                    let b_val = self.get_from_program(opcode, 1);
                    let c_val = self.get_from_program(opcode, 2);
                    self.write_to_program(a, opcode, 3, if b_val == c_val {1} else {0});
                    self.instruction_pointer += 4;

                }
                9 => {
                    self.relative_base = (self.relative_base + self.get_from_program(opcode, 1)).try_into().unwrap();
                    self.instruction_pointer += 2;
                }
                99 => {
                    self.halted = true;
                    return Option::None;
                }
                _ => panic!("Unexpected opcode")
            }
        }
    }
}
