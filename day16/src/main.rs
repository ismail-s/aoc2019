use std::fs;

fn main() -> Result<(), std::io::Error> {
    println!("Part 1");
    part1()?;
    println!("Part 2");
    part2()
}

fn part1() -> Result<(), std::io::Error> {
    let file_contents = std::env::args().nth(1)
        .and_then(|filename| fs::read_to_string(filename).ok())
        .expect("Need to be passed valid filename as first argument");
    let input_digits = file_contents.trim().chars()
        .map(|c| c.to_digit(10))
        .collect::<Option<Vec<u32>>>()
        .expect("Couldn't parse file contents as digits");
    let base_pattern = vec![0, 1, 0, -1];

    let mut output_digits = input_digits.clone();

    for _ in 0..100 {
        output_digits = run_fft(&output_digits, &base_pattern);
    }
    println!("First 8 digits are {:?}", output_digits.iter().cloned().take(8).collect::<Vec<u32>>());
    Ok(())
}

fn part2() -> Result<(), std::io::Error> {
    let file_contents = std::env::args().nth(1)
        .and_then(|filename| fs::read_to_string(filename).ok())
        .expect("Need to be passed valid filename as first argument");
    let input_digits = file_contents.trim().chars()
        .map(|c| c.to_digit(10))
        .collect::<Option<Vec<u32>>>()
        .expect("Couldn't parse file contents as digits");

    let message_offset = input_digits.iter().take(7).map(|n| format!("{}", n)).collect::<String>().parse::<usize>().unwrap();

    // If our message_offset gets rid of over half of our input
    assert!(message_offset * 2 > input_digits.len() * 10_000);
    // then from base_pattern ([0, 1, 0, -1]), only 0, 1 are actually used
    // and the fft function is just a backwards running total modulo 10.

    let output_digits: Vec<u32> = (0..10_000_i32).flat_map(|_| input_digits.iter()).skip(message_offset).cloned().collect();
    // We reverse the digits so we can iterate backwards
    let mut r_output_digits: Vec<u32> = output_digits.iter().rev().cloned().collect();

    for _ in 0..100 {
        // Compute running total, store results using in-place mutation
        let mut i = 0;
        for elem in r_output_digits.iter_mut() {
            i += *elem;
            i %= 10;
            *elem = i;
        }
    }
    println!("First 8 digits are {:?}", r_output_digits.iter().rev().take(8).cloned().collect::<Vec<u32>>());
    Ok(())
}

fn run_fft(input_digits: &[u32], base_pattern: &[i32]) -> Vec<u32> {
    input_digits.iter().enumerate().map(|(i, _)| {
        let num: i32 = base_pattern.iter()
            .flat_map(|base_num| std::iter::repeat(base_num).take(i + 1)).cycle().skip(1)
            .zip(input_digits)
            .map(|(&a, &b)|a * (b as i32))
            .sum();
        (num.abs() % 10) as u32
    }).collect()
}
