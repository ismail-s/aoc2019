use std::collections::HashSet;
use num::Integer;

fn main() -> Result<(), std::io::Error> {
    println!("Part 1");
    part1()?;
    println!("Part 2");
    part2()
}

fn part1() -> Result<(), std::io::Error> {
    let moon1 = Moon::new(-9, 10, -1);
    let moon2 = Moon::new(-14, -8, 14);
    let moon3 = Moon::new(1, 5, 6);
    let moon4 = Moon::new(-19, 7, 8);

    let mut system = System::new(moon1, moon2, moon3, moon4);

    for _ in 0..1000 {
        system.step();
    }

    println!("Total energy after 1000 steps is {}", system.energy());
    Ok(())
}

fn part2() -> Result<(), std::io::Error> {
    let cycle1 = compute_cycle(-9, -14, 1, -19);
    let cycle2 = compute_cycle(10, -8, 5, 7);
    let cycle3 = compute_cycle(-1, 14, 6, 8);

    println!("Cycles for each axes are {} {} {}", cycle1, cycle2, cycle3);
    println!("Lowest common multiple of them is {}", cycle1.lcm(&cycle2).lcm(&cycle3));
    Ok(())
}

fn compute_cycle(mut p1: i64, mut p2: i64, mut p3: i64, mut p4: i64) -> i64 {
    let mut vel1 = 0;
    let mut vel2 = 0;
    let mut vel3 = 0;
    let mut vel4 = 0;

    let mut set = HashSet::new();

    set.insert(format!("{},{},{},{},{},{},{},{}", p1, p2, p3, p4, vel1, vel2, vel3, vel4));

    for i in 1.. {
        vel1 += compute_gravity(p1, p2, p3, p4);
        vel2 += compute_gravity(p2, p1, p3, p4);
        vel3 += compute_gravity(p3, p2, p1, p4);
        vel4 += compute_gravity(p4, p2, p3, p1);

        p1 += vel1;
        p2 += vel2;
        p3 += vel3;
        p4 += vel4;

        let res = format!("{},{},{},{},{},{},{},{}", p1, p2, p3, p4, vel1, vel2, vel3, vel4);

        if set.contains(&res) {
            assert_eq!(0, vel1);
            assert_eq!(0, vel2);
            assert_eq!(0, vel3);
            assert_eq!(0, vel4);
            return i;
        }

        set.insert(res);
    }
    panic!("Somehow managed to break out of an infinite loop");
}


struct Moon {
    x: i64,
    y: i64,
    z: i64,
    x_vel: i64,
    y_vel: i64,
    z_vel: i64
}
impl Moon {
    fn new(x: i64, y: i64, z: i64) -> Moon {
        Moon {
            x, y, z,
            x_vel: 0,
            y_vel: 0,
            z_vel: 0
        }
    }
}

struct System {
    steps: u64,
    moon1: Moon,
    moon2: Moon,
    moon3: Moon,
    moon4: Moon
}

impl System {
    fn new(moon1: Moon, moon2: Moon, moon3: Moon, moon4: Moon) -> System {
        System {
            steps: 0,
            moon1, moon2, moon3, moon4
        }
    }

    fn energy(&self) -> i64 {
        let pos_total1 = self.moon1.x.abs() + self.moon1.y.abs() + self.moon1.z.abs();
        let vel_total1 = self.moon1.x_vel.abs() + self.moon1.y_vel.abs() + self.moon1.z_vel.abs();
        let total1 = pos_total1 * vel_total1;

        let pos_total2 = self.moon2.x.abs() + self.moon2.y.abs() + self.moon2.z.abs();
        let vel_total2 = self.moon2.x_vel.abs() + self.moon2.y_vel.abs() + self.moon2.z_vel.abs();
        let total2 = pos_total2 * vel_total2;

        let pos_total3 = self.moon3.x.abs() + self.moon3.y.abs() + self.moon3.z.abs();
        let vel_total3 = self.moon3.x_vel.abs() + self.moon3.y_vel.abs() + self.moon3.z_vel.abs();
        let total3 = pos_total3 * vel_total3;

        let pos_total4 = self.moon4.x.abs() + self.moon4.y.abs() + self.moon4.z.abs();
        let vel_total4 = self.moon4.x_vel.abs() + self.moon4.y_vel.abs() + self.moon4.z_vel.abs();
        let total4 = pos_total4 * vel_total4;

        total1 + total2 + total3 + total4
    }

    fn step(&mut self) {
        self.moon1.x_vel += compute_gravity(self.moon1.x, self.moon2.x, self.moon3.x, self.moon4.x);
        self.moon1.y_vel += compute_gravity(self.moon1.y, self.moon2.y, self.moon3.y, self.moon4.y);
        self.moon1.z_vel += compute_gravity(self.moon1.z, self.moon2.z, self.moon3.z, self.moon4.z);

        self.moon2.x_vel += compute_gravity(self.moon2.x, self.moon1.x, self.moon3.x, self.moon4.x);
        self.moon2.y_vel += compute_gravity(self.moon2.y, self.moon1.y, self.moon3.y, self.moon4.y);
        self.moon2.z_vel += compute_gravity(self.moon2.z, self.moon1.z, self.moon3.z, self.moon4.z);

        self.moon3.x_vel += compute_gravity(self.moon3.x, self.moon2.x, self.moon1.x, self.moon4.x);
        self.moon3.y_vel += compute_gravity(self.moon3.y, self.moon2.y, self.moon1.y, self.moon4.y);
        self.moon3.z_vel += compute_gravity(self.moon3.z, self.moon2.z, self.moon1.z, self.moon4.z);

        self.moon4.x_vel += compute_gravity(self.moon4.x, self.moon2.x, self.moon3.x, self.moon1.x);
        self.moon4.y_vel += compute_gravity(self.moon4.y, self.moon2.y, self.moon3.y, self.moon1.y);
        self.moon4.z_vel += compute_gravity(self.moon4.z, self.moon2.z, self.moon3.z, self.moon1.z);

        self.moon1.x += self.moon1.x_vel;
        self.moon1.y += self.moon1.y_vel;
        self.moon1.z += self.moon1.z_vel;

        self.moon2.x += self.moon2.x_vel;
        self.moon2.y += self.moon2.y_vel;
        self.moon2.z += self.moon2.z_vel;

        self.moon3.x += self.moon3.x_vel;
        self.moon3.y += self.moon3.y_vel;
        self.moon3.z += self.moon3.z_vel;

        self.moon4.x += self.moon4.x_vel;
        self.moon4.y += self.moon4.y_vel;
        self.moon4.z += self.moon4.z_vel;

        self.steps += 1;
    }
}

fn compute_gravity(base: i64, one: i64, two: i64, three: i64) -> i64 {
    let mut res = if base > one {-1} else if base < one {1} else {0};
    res += if base > two {-1} else if base < two {1} else {0};
    res += if base > three {-1} else if base < three {1} else {0};
    res
}
