use std::vec::Vec;

fn main() -> Result<(), std::io::Error> {
    println!("Part One");
    part1()?;
    println!("Part Two");
    part2()
}

fn part1() -> Result<(), std::io::Error> {
    let range = (134564, 585159);
    let it = range.0..(range.1 + 1);
    let count = it.map(|num| format!("{}", num)).map(|s| s.chars().map(|c| c.to_digit(10).unwrap()).collect::<Vec<u32>>())
        .filter(|v| v[0] == v[1] || v[1] == v[2] || v[2] == v[3] || v[3] == v[4] || v[4] == v[5])
        .filter(|v| v[0] <= v[1] && v[1] <= v[2] && v[2] <= v[3] && v[3] <= v[4] && v[4] <= v[5])
        .count();
    println!("Number of different possible passwords in given range: {}", count);

    Ok(())
}

fn part2() -> Result<(), std::io::Error> {
    let range = (134564, 585159);
    let it = range.0..(range.1 + 1);
    let count = it.map(|num| format!("{}", num)).map(|s| s.chars().map(|c| c.to_digit(10).unwrap()).collect::<Vec<u32>>())
        .filter(|v|
                (v[0] == v[1] && v[2] != v[1]) ||
                (v[1] == v[2] && v[0] != v[1] && v[3] != v[1]) ||
                (v[2] == v[3] && v[1] != v[2] && v[4] != v[2]) ||
                (v[3] == v[4] && v[2] != v[3] && v[5] != v[3]) ||
                (v[4] == v[5] && v[3] != v[4]))
        .filter(|v| v[0] <= v[1] && v[1] <= v[2] && v[2] <= v[3] && v[3] <= v[4] && v[4] <= v[5])
        .count();
    println!("Number of different possible passwords in given range (with additional criteria): {}", count);

    Ok(())
}
