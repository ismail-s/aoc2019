use std::fs;
use std::collections::HashSet;
use gcd::Gcd;

fn main() -> Result<(), std::io::Error> {
    let file_contents = std::env::args().nth(1)
        .and_then(|filename| fs::read_to_string(filename).ok())
        .expect("Need to be passed valid filename as first argument");
    let grid = file_contents.trim().lines()
        .map(|line| line.chars()
             .map(|c| c == '#')
             .collect::<Vec<bool>>())
        .collect::<Vec<_>>();

    let mut asteroids = HashSet::new();
    for (j, row) in grid.iter().enumerate() {
        for (i, &value) in row.iter().enumerate() {
            if value {
                asteroids.insert((i, j));
            }
        }
    } // This could be combined with the maps above

    let mut max_count = 0;
    let mut max_pos = (0, 0);

    println!("Part Two");

    for (a_i, a_j) in &asteroids {
        let mut count = 0;
        for (b_i, b_j) in &asteroids {
            let points = compute_points_between(*a_i, *a_j, *b_i, *b_j);
            if asteroids.intersection(&points).next().is_none() {
                count += 1;
            }
        }
        if count > max_count {
            max_count = count;
            max_pos = (*a_i, *a_j);
        }
    }
    println!("Best place for monitoring station is {:?}.", max_pos);
    println!("{} asteroids can be detected from there.", max_count);

    println!("Part Two");

    let monitoring_station = max_pos;
    let mut asteroid_count = 0;

    assert_eq!(true, asteroids.remove(&monitoring_station));

    // Find all asteroids that are nearest
    // If their count + asteroid_count < 200 (check for out-by-one error)
    //  Remove all those asteroids
    //  Else, sort them by angle, find the 200th one, print and exit
    loop {
        let mut closest_asteroids = HashSet::new();
        for (b_i, b_j) in &asteroids {
            let points = compute_points_between(monitoring_station.0, monitoring_station.1, *b_i, *b_j);
            if asteroids.intersection(&points).next().is_none() {
                closest_asteroids.insert((*b_i, *b_j));
            }
        }
        if asteroid_count + closest_asteroids.len() < 200 {
            asteroid_count += closest_asteroids.len();
            for asteroid in closest_asteroids.iter().cloned() {
                asteroids.remove(&(asteroid.0, asteroid.1));
            }
        } else {
            let mut c  = closest_asteroids.iter().collect::<Vec<_>>();
            c.sort_unstable_by_key(|(i, j)| compute_angle(*i, *j, monitoring_station.0, monitoring_station.1));
            let res = c.get(200 - 1 - asteroid_count);
            println!("200th coordinate is {:?}", res);
            return Ok(());
        }
    }
}

fn compute_angle(a_i: usize, a_j: usize, b_i: usize, b_j: usize) -> usize {
    let i = (a_i as isize) - (b_i as isize);
    let j = (b_j as isize) - (a_j as isize); // This is deliberately flipped compared to the previous line
    let i = i as f64;
    let j = j as f64;
    let x = -j;
    let y = i;
    let n = (std::f64::consts::PI * 2.0) - (y.atan2(x) + std::f64::consts::PI);
    ((n * 360.0) / std::f64::consts::PI * 2.0) as usize
}

fn compute_points_between(a_i: usize, a_j: usize, b_i: usize, b_j: usize) -> HashSet<(usize, usize)> {
    let mut ret = HashSet::new();
    if a_i == b_i && a_j == b_j {
        return [(a_i, a_j)].iter().cloned().collect();
    }
    let divisor = (((b_i as isize) - (a_i as isize)).abs() as usize).gcd(((b_j as isize) - (a_j as isize)).abs() as usize);
    let i_diff = ((b_i as isize) - (a_i as isize)) / (divisor as isize);
    let j_diff = ((b_j as isize) - (a_j as isize)) / (divisor as isize);
    for mult_factor in (1 as isize)..(divisor as isize) {
        let (i, j) = ((a_i as isize) + (mult_factor * i_diff), (a_j as isize) + (mult_factor * j_diff));
        ret.insert((i as usize, j as usize));
    }
    ret
}
