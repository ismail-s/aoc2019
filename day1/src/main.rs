use std::fs;

fn main() -> Result<(), std::io::Error> {
    let file_contents = std::env::args().nth(1)
        .and_then(|filename| fs::read_to_string(filename).ok())
        .expect("Need to be passed valid filename as first argument");
    let module_masses = file_contents.lines()
        .map(|line| line.parse::<i32>())
        .collect::<Result<Vec<_>, _>>()
        .expect("Couldn't parse string as int");

    let sum: i32 = module_masses.iter().map(|&n| fuel_for_module(n)).sum();
    println!("First part:");
    println!("Sum is {}", sum);

    let sum: i32 = module_masses.iter().map(|&n| recursive_fuel_for_module(n)).sum();
    println!("Second part:");
    println!("Sum is {}", sum);
    Ok(())
}

fn fuel_for_module(mass: i32) -> i32 {
    (mass / 3) - 2
}

fn recursive_fuel_for_module(mass: i32) -> i32 {
    let fuel_for_mass = fuel_for_module(mass);
    if fuel_for_mass < 0 {
        0
    } else {
        fuel_for_mass + recursive_fuel_for_module(fuel_for_mass)
    }
}
