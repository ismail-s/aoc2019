use std::fs;
use std::collections::HashMap;

fn main() -> Result<(), std::io::Error> {
    let file_contents = std::env::args().nth(1)
        .and_then(|filename| fs::read_to_string(filename).ok())
        .expect("Need to be passed valid filename as first argument");
    let image = file_contents.trim().chars()
        .map(|num| num.to_digit(10))
        .collect::<Option<Vec<_>>>()
        .expect("Couldn't parse string as int");

    let width = 25;
    let height = 6;
    let pixels_in_image = width * height;

    assert_eq!(0, image.len() % pixels_in_image);

    println!("Part One");

    let mut map: HashMap<usize, usize> = HashMap::new();

    for layer in image.chunks(pixels_in_image) {
        let get_num_of = |m| layer.iter().filter(|&n| *n == m).count();
        let zeros = get_num_of(0);
        let ones = get_num_of(1);
        let twos = get_num_of(2);
        map.insert(zeros, ones * twos);
    }
    let fewest_zeros = map.keys().min().expect("Should be at least one layer in image");
    println!("Checksum: {}", map[fewest_zeros]);

    println!("Part Two");

    let num_of_layers = image.len() / pixels_in_image;

    for i in 0..pixels_in_image {
        if i % width == 0 {
            println!();
        }
        let pixel = (0..num_of_layers)
            .map(|layer_i| image[(pixels_in_image * layer_i) + i])
            .fold(2, |acc, x| if acc == 2 {x} else {acc});
        match pixel {
            0 => print!(" "),
            1 => print!("█"),
            2 => panic!("Only empty pixels found in pixel {}", i),
            _ => panic!("Unexpected value found in pixel {}", i)
        }
    }

    Ok(())
}
