use std::fs;
use std::convert::TryFrom;

fn main() -> Result<(), std::io::Error> {
    println!("Part One");
    part1()?;
    println!("Part Two");
    part2()
}

fn part1() -> Result<(), std::io::Error> {
    let file_contents = std::env::args().nth(1)
        .and_then(|filename| fs::read_to_string(filename).ok())
        .expect("Need to be passed valid filename as first argument");
    let mut program = file_contents.trim().split(',')
        .map(|num| num.parse::<i32>())
        .collect::<Result<Vec<_>, _>>()
        .expect("Couldn't parse string as int");

    let mut instruction_pointer = 0;

    loop {
        let opcode = program[instruction_pointer];
        let bb = (opcode % 1000) / 100;
        let cc = (opcode % 10000) / 1000;
        match opcode % 100 {
            1 => {
                let a = program[instruction_pointer + 3];
                let b = program[instruction_pointer + 1];
                let c = program[instruction_pointer + 2];
                let b_val = if bb == 0 {get_from_program(&program, b)} else {b};
                let c_val = if cc == 0 {get_from_program(&program, c)} else {c};
                program[convert_index(a)] = b_val + c_val;
                instruction_pointer += 4;
            }
            2 => {
                let a = program[instruction_pointer + 3];
                let b = program[instruction_pointer + 1];
                let c = program[instruction_pointer + 2];
                let b_val = if bb == 0 {get_from_program(&program, b)} else {b};
                let c_val = if cc == 0 {get_from_program(&program, c)} else {c};
                program[convert_index(a)] = b_val * c_val;
                instruction_pointer += 4;
            }
            3 => {
                let b = program[instruction_pointer + 1];
                program[convert_index(b)] = 1; // Input value 1
                instruction_pointer += 2;
            }
            4 => {
                let b = program[instruction_pointer + 1];
                let b_val = if bb == 0 {get_from_program(&program, b)} else {b};
                println!("Outputting value {} at instruction pointer {}", b_val, instruction_pointer);
                instruction_pointer += 2;
            }
            99 => {
                println!("Program has finished.");
                println!("Value at position 0: {}", program[0]);
                break;
            }
            _ => panic!("Unexpected opcode")
        }
    }

    Ok(())
}

fn part2() -> Result<(), std::io::Error> {
    let file_contents = std::env::args().nth(1)
        .and_then(|filename| fs::read_to_string(filename).ok())
        .expect("Need to be passed valid filename as first argument");
    let mut program = file_contents.trim().split(',')
        .map(|num| num.parse::<i32>())
        .collect::<Result<Vec<_>, _>>()
        .expect("Couldn't parse string as int");

    let mut instruction_pointer = 0;

    loop {
        let opcode = program[instruction_pointer];
        let bb = (opcode % 1000) / 100;
        let cc = (opcode % 10000) / 1000;
        match opcode % 100 {
            1 => {
                let a = program[instruction_pointer + 3];
                let b = program[instruction_pointer + 1];
                let c = program[instruction_pointer + 2];
                let b_val = if bb == 0 {get_from_program(&program, b)} else {b};
                let c_val = if cc == 0 {get_from_program(&program, c)} else {c};
                program[convert_index(a)] = b_val + c_val;
                instruction_pointer += 4;
            }
            2 => {
                let a = program[instruction_pointer + 3];
                let b = program[instruction_pointer + 1];
                let c = program[instruction_pointer + 2];
                let b_val = if bb == 0 {get_from_program(&program, b)} else {b};
                let c_val = if cc == 0 {get_from_program(&program, c)} else {c};
                program[convert_index(a)] = b_val * c_val;
                instruction_pointer += 4;
            }
            3 => {
                let b = program[instruction_pointer + 1];
                program[convert_index(b)] = 5; // Input value 5
                instruction_pointer += 2;
            }
            4 => {
                let b = program[instruction_pointer + 1];
                let b_val = if bb == 0 {get_from_program(&program, b)} else {b};
                println!("Outputting value {} at instruction pointer {}", b_val, instruction_pointer);
                instruction_pointer += 2;
            }
            5 => {
                let b = program[instruction_pointer + 1];
                let c = program[instruction_pointer + 2];
                let b_val = if bb == 0 {get_from_program(&program, b)} else {b};
                let c_val = if cc == 0 {get_from_program(&program, c)} else {c};
                if b_val != 0 {
                    instruction_pointer = convert_index(c_val);
                } else {
                    instruction_pointer += 3;
                }
            }
            6 => {
                let b = program[instruction_pointer + 1];
                let c = program[instruction_pointer + 2];
                let b_val = if bb == 0 {get_from_program(&program, b)} else {b};
                let c_val = if cc == 0 {get_from_program(&program, c)} else {c};
                if b_val == 0 {
                    instruction_pointer = convert_index(c_val);
                } else {
                    instruction_pointer += 3;
                }
            }
            7 => {
                let a = program[instruction_pointer + 3];
                let b = program[instruction_pointer + 1];
                let c = program[instruction_pointer + 2];
                let b_val = if bb == 0 {get_from_program(&program, b)} else {b};
                let c_val = if cc == 0 {get_from_program(&program, c)} else {c};
                program[convert_index(a)] = if b_val < c_val {1} else {0};
                instruction_pointer += 4;

            }
            8 => {
                let a = program[instruction_pointer + 3];
                let b = program[instruction_pointer + 1];
                let c = program[instruction_pointer + 2];
                let b_val = if bb == 0 {get_from_program(&program, b)} else {b};
                let c_val = if cc == 0 {get_from_program(&program, c)} else {c};
                program[convert_index(a)] = if b_val == c_val {1} else {0};
                instruction_pointer += 4;

            }
            99 => {
                println!("Program has finished.");
                println!("Value at position 0: {}", program[0]);
                break;
            }
            _ => panic!("Unexpected opcode")
        }
    }

    Ok(())
}

fn get_from_program(program: &[i32], index: i32) -> i32 {
    program[convert_index(index)]
}

fn convert_index(index: i32) -> usize {
    usize::try_from(index).expect("Couldn't convert index from i32 to usize")
}
