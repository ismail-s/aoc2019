use std::fs;

fn main() -> Result<(), std::io::Error> {
    println!("Part One");
    part1()?;
    println!("Part Two");
    part2()
}

fn part1() -> Result<(), std::io::Error> {
    let file_contents = std::env::args().nth(1)
        .and_then(|filename| fs::read_to_string(filename).ok())
        .expect("Need to be passed valid filename as first argument");
    let mut program = file_contents.trim().split(',')
        .map(|num| num.parse::<usize>())
        .collect::<Result<Vec<_>, _>>()
        .expect("Couldn't parse string as int");

    let mut instruction_pointer = 0;
    program[1] = 12;
    program[2] = 2;

    loop {
        let opcode = program[instruction_pointer];
        if opcode == 1 {
            let a = program[instruction_pointer + 3];
            let b = program[instruction_pointer + 1];
            let c = program[instruction_pointer + 2];
            program[a] = program[b] + program[c];
        } else if opcode == 2 {
            let a = program[instruction_pointer + 3];
            let b = program[instruction_pointer + 1];
            let c = program[instruction_pointer + 2];
            program[a] = program[b] * program[c];
        } else if opcode == 99 {
            println!("Program has finished.");
            println!("Value at position 0: {}", program[0]);
            break;
        }
        instruction_pointer += 4;
    }

    Ok(())
}

fn part2() -> Result<(), std::io::Error> {
    for noun in 0..100 {
        for verb in 0..100 {
            let file_contents = std::env::args().nth(1)
                .and_then(|filename| fs::read_to_string(filename).ok())
                .expect("Need to be passed valid filename as first argument");
            let mut program = file_contents.trim().split(',')
                .map(|num| num.parse::<usize>())
                .collect::<Result<Vec<_>, _>>()
                .expect("Couldn't parse string as int");

            let mut instruction_pointer = 0;
            program[1] = noun;
            program[2] = verb;

            loop {
                let opcode = program[instruction_pointer];
                if opcode == 1 {
                    let a = program[instruction_pointer + 3];
                    let b = program[instruction_pointer + 1];
                    let c = program[instruction_pointer + 2];
                    program[a] = program[b] + program[c];
                } else if opcode == 2 {
                    let a = program[instruction_pointer + 3];
                    let b = program[instruction_pointer + 1];
                    let c = program[instruction_pointer + 2];
                    program[a] = program[b] * program[c];
                } else if opcode == 99 {
                    break;
                }
                instruction_pointer += 4;
            }
            if program[0] == 19690720 {
                println!("Got the number with noun {} and verb {}", noun, verb);
                println!("Final answer is then {}", (noun * 100) + verb);
            }
        }
    }

    Ok(())
}
