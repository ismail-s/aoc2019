use std::fs;
use std::convert::TryFrom;
use std::collections::HashSet;

fn main() -> Result<(), std::io::Error> {
    println!("Part One");
    part1()?;
    println!("Part Two");
    part2()
}

fn part1() -> Result<(), std::io::Error> {
    let file_contents = std::env::args().nth(1)
        .and_then(|filename| fs::read_to_string(filename).ok())
        .expect("Need to be passed valid filename as first argument");
    let program = file_contents.trim().split(',')
        .map(|num| num.parse::<i32>())
        .collect::<Result<Vec<_>, _>>()
        .expect("Couldn't parse string as int");

    let mut result = 0;
    let mut phase_settings = (0, 0, 0, 0, 0);
    for a in 0..5 {
        for b in 0..5 {
            for c in 0..5 {
                for d in 0..5 {
                    for e in 0..5 {
                        let mut s = HashSet::with_capacity(5);
                        s.insert(a);
                        s.insert(b);
                        s.insert(c);
                        s.insert(d);
                        s.insert(e);
                        if s.len() == 5 {
                            let res = run_five_amplifiers(&program, &[a, b, c, d, e]);
                            if res > result {
                                result = res;
                                phase_settings = (a, b, c, d, e);
                            }
                        }
                    }
                }
            }
        }
    }
    println!("Phase settings: {:?}", phase_settings);
    println!("Highest possible signal: {}", result);

    Ok(())
}

fn get_from_program(program: &[i32], index: i32) -> i32 {
    program[convert_index(index)]
}

fn convert_index(index: i32) -> usize {
    usize::try_from(index).expect("Couldn't convert index from i32 to usize")
}

fn run_five_amplifiers(program: &[i32], phase_settings: &[i32]) -> i32 {
    let outputs = run_program(&program, &[phase_settings[0], 0]);
    let outputs = run_program(&program, &[phase_settings[1], outputs[0]]);
    let outputs = run_program(&program, &[phase_settings[2], outputs[0]]);
    let outputs = run_program(&program, &[phase_settings[3], outputs[0]]);
    run_program(&program, &[phase_settings[4], outputs[0]])[0]
}

fn run_program(program1: &[i32], inputs: &[i32]) -> Vec<i32> {
    let mut program = Vec::new();
    program.extend(program1.iter());
    let mut input_pointer = 0;
    let mut instruction_pointer = 0;
    let mut outputs = Vec::new();

    loop {
        let opcode = program[instruction_pointer];
        let bb = (opcode % 1000) / 100;
        let cc = (opcode % 10000) / 1000;
        match opcode % 100 {
            1 => {
                let a = program[instruction_pointer + 3];
                let b = program[instruction_pointer + 1];
                let c = program[instruction_pointer + 2];
                let b_val = if bb == 0 {get_from_program(&program, b)} else {b};
                let c_val = if cc == 0 {get_from_program(&program, c)} else {c};
                program[convert_index(a)] = b_val + c_val;
                instruction_pointer += 4;
            }
            2 => {
                let a = program[instruction_pointer + 3];
                let b = program[instruction_pointer + 1];
                let c = program[instruction_pointer + 2];
                let b_val = if bb == 0 {get_from_program(&program, b)} else {b};
                let c_val = if cc == 0 {get_from_program(&program, c)} else {c};
                program[convert_index(a)] = b_val * c_val;
                instruction_pointer += 4;
            }
            3 => {
                let b = program[instruction_pointer + 1];
                program[convert_index(b)] = *inputs.get(input_pointer)
                    .expect("Program tried to get input that wasn't provided.");
                input_pointer += 1;
                instruction_pointer += 2;
            }
            4 => {
                let b = program[instruction_pointer + 1];
                let b_val = if bb == 0 {get_from_program(&program, b)} else {b};
                outputs.push(b_val);
                instruction_pointer += 2;
            }
            5 => {
                let b = program[instruction_pointer + 1];
                let c = program[instruction_pointer + 2];
                let b_val = if bb == 0 {get_from_program(&program, b)} else {b};
                let c_val = if cc == 0 {get_from_program(&program, c)} else {c};
                if b_val != 0 {
                    instruction_pointer = convert_index(c_val);
                } else {
                    instruction_pointer += 3;
                }
            }
            6 => {
                let b = program[instruction_pointer + 1];
                let c = program[instruction_pointer + 2];
                let b_val = if bb == 0 {get_from_program(&program, b)} else {b};
                let c_val = if cc == 0 {get_from_program(&program, c)} else {c};
                if b_val == 0 {
                    instruction_pointer = convert_index(c_val);
                } else {
                    instruction_pointer += 3;
                }
            }
            7 => {
                let a = program[instruction_pointer + 3];
                let b = program[instruction_pointer + 1];
                let c = program[instruction_pointer + 2];
                let b_val = if bb == 0 {get_from_program(&program, b)} else {b};
                let c_val = if cc == 0 {get_from_program(&program, c)} else {c};
                program[convert_index(a)] = if b_val < c_val {1} else {0};
                instruction_pointer += 4;

            }
            8 => {
                let a = program[instruction_pointer + 3];
                let b = program[instruction_pointer + 1];
                let c = program[instruction_pointer + 2];
                let b_val = if bb == 0 {get_from_program(&program, b)} else {b};
                let c_val = if cc == 0 {get_from_program(&program, c)} else {c};
                program[convert_index(a)] = if b_val == c_val {1} else {0};
                instruction_pointer += 4;

            }
            99 => {
                break outputs;
            }
            _ => panic!("Unexpected opcode")
        }
    }
}

fn part2() -> Result<(), std::io::Error> {
    let file_contents = std::env::args().nth(1)
        .and_then(|filename| fs::read_to_string(filename).ok())
        .expect("Need to be passed valid filename as first argument");
    let program = file_contents.trim().split(',')
        .map(|num| num.parse::<i32>())
        .collect::<Result<Vec<_>, _>>()
        .expect("Couldn't parse string as int");

    let mut result = 0;
    let mut phase_settings = (0, 0, 0, 0, 0);
    for a in 5..10 {
        for b in 5..10 {
            for c in 5..10 {
                for d in 5..10 {
                    for e in 5..10 {
                        let mut s = HashSet::with_capacity(5);
                        s.insert(a);
                        s.insert(b);
                        s.insert(c);
                        s.insert(d);
                        s.insert(e);
                        if s.len() == 5 {
                            let res = run_five_amplifiers_looped(&program, &[a, b, c, d, e]);
                            if res > result {
                                result = res;
                                phase_settings = (a, b, c, d, e);
                            }
                        }
                    }
                }
            }
        }
    }
    println!("Phase settings: {:?}", phase_settings);
    println!("Highest possible signal looped: {}", result);

    Ok(())
}

fn run_five_amplifiers_looped(program: &[i32], phase_settings: &[i32]) -> i32 {
    let mut amp_1 = Amplifier::new(program);
    let mut amp_2 = Amplifier::new(program);
    let mut amp_3 = Amplifier::new(program);
    let mut amp_4 = Amplifier::new(program);
    let mut amp_5 = Amplifier::new(program);
    let mut input = 0;
    let res = amp_1.run_program_to_output(&[phase_settings[0], input])
        .and_then(|n| amp_2.run_program_to_output(&[phase_settings[1], n]))
        .and_then(|n| amp_3.run_program_to_output(&[phase_settings[2], n]))
        .and_then(|n| amp_4.run_program_to_output(&[phase_settings[3], n]))
        .and_then(|n| amp_5.run_program_to_output(&[phase_settings[4], n]));
    match res {
        None => return input,
        Some(new_input) => {
            input = new_input
        }
    }
    loop {
        let res = amp_1.run_program_to_output(&[input])
            .and_then(|n| amp_2.run_program_to_output(&[n]))
            .and_then(|n| amp_3.run_program_to_output(&[n]))
            .and_then(|n| amp_4.run_program_to_output(&[n]))
            .and_then(|n| amp_5.run_program_to_output(&[n]));
        match res {
            None => return input,
            Some(new_input) => {
                input = new_input
            }
        }
    }
}

struct Amplifier {
    program: Vec<i32>,
    instruction_pointer: usize,
    halted: bool
}

impl Amplifier {
    fn new(program: &[i32]) -> Amplifier {
        let mut program_vec = Vec::new();
        for p in program {
            program_vec.push(*p);
        }
        Amplifier {
            program: program_vec,
            instruction_pointer: 0,
            halted: false
        }
    }

    fn run_program_to_output(&mut self, inputs: &[i32]) -> Option<i32> {
        if self.halted {return Option::None}
        let mut input_pointer = 0;

        loop {
            let opcode = self.program[self.instruction_pointer];
            let bb = (opcode % 1000) / 100;
            let cc = (opcode % 10000) / 1000;
            match opcode % 100 {
                1 => {
                    let a = self.program[self.instruction_pointer + 3];
                    let b = self.program[self.instruction_pointer + 1];
                    let c = self.program[self.instruction_pointer + 2];
                    let b_val = if bb == 0 {get_from_program(&self.program, b)} else {b};
                    let c_val = if cc == 0 {get_from_program(&self.program, c)} else {c};
                    self.program[convert_index(a)] = b_val + c_val;
                    self.instruction_pointer += 4;
                }
                2 => {
                    let a = self.program[self.instruction_pointer + 3];
                    let b = self.program[self.instruction_pointer + 1];
                    let c = self.program[self.instruction_pointer + 2];
                    let b_val = if bb == 0 {get_from_program(&self.program, b)} else {b};
                    let c_val = if cc == 0 {get_from_program(&self.program, c)} else {c};
                    self.program[convert_index(a)] = b_val * c_val;
                    self.instruction_pointer += 4;
                }
                3 => {
                    let b = self.program[self.instruction_pointer + 1];
                    self.program[convert_index(b)] = *inputs.get(input_pointer)
                        .expect("Program tried to get input that wasn't provided.");
                    input_pointer += 1;
                    self.instruction_pointer += 2;
                }
                4 => {
                    let b = self.program[self.instruction_pointer + 1];
                    let b_val = if bb == 0 {get_from_program(&self.program, b)} else {b};
                    self.instruction_pointer += 2;
                    return Option::Some(b_val);
                }
                5 => {
                    let b = self.program[self.instruction_pointer + 1];
                    let c = self.program[self.instruction_pointer + 2];
                    let b_val = if bb == 0 {get_from_program(&self.program, b)} else {b};
                    let c_val = if cc == 0 {get_from_program(&self.program, c)} else {c};
                    if b_val != 0 {
                        self.instruction_pointer = convert_index(c_val);
                    } else {
                        self.instruction_pointer += 3;
                    }
                }
                6 => {
                    let b = self.program[self.instruction_pointer + 1];
                    let c = self.program[self.instruction_pointer + 2];
                    let b_val = if bb == 0 {get_from_program(&self.program, b)} else {b};
                    let c_val = if cc == 0 {get_from_program(&self.program, c)} else {c};
                    if b_val == 0 {
                        self.instruction_pointer = convert_index(c_val);
                    } else {
                        self.instruction_pointer += 3;
                    }
                }
                7 => {
                    let a = self.program[self.instruction_pointer + 3];
                    let b = self.program[self.instruction_pointer + 1];
                    let c = self.program[self.instruction_pointer + 2];
                    let b_val = if bb == 0 {get_from_program(&self.program, b)} else {b};
                    let c_val = if cc == 0 {get_from_program(&self.program, c)} else {c};
                    self.program[convert_index(a)] = if b_val < c_val {1} else {0};
                    self.instruction_pointer += 4;

                }
                8 => {
                    let a = self.program[self.instruction_pointer + 3];
                    let b = self.program[self.instruction_pointer + 1];
                    let c = self.program[self.instruction_pointer + 2];
                    let b_val = if bb == 0 {get_from_program(&self.program, b)} else {b};
                    let c_val = if cc == 0 {get_from_program(&self.program, c)} else {c};
                    self.program[convert_index(a)] = if b_val == c_val {1} else {0};
                    self.instruction_pointer += 4;

                }
                99 => {
                    self.halted = true;
                    return Option::None;
                }
                _ => panic!("Unexpected opcode")
            }
        }
    }
}
